/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __38e6873828280cee981ea0b8289d7971__
#define __38e6873828280cee981ea0b8289d7971__

#include <yip-imports/glm/glm.hpp>
#include <yip-imports/math/ray.h>
#include <memory>
#include <vector>

class SceneNode;
class SceneCamera;

typedef std::shared_ptr<SceneNode> SceneNodePtr;
typedef std::weak_ptr<SceneNode> SceneNodeWeakPtr;

class SceneNode : public std::enable_shared_from_this<SceneNode>
{
public:
	SceneNode();
	virtual ~SceneNode();

	inline SceneNodePtr parent() const noexcept { return m_Parent.lock(); }

	void addChild(const SceneNodePtr & child);
	void removeChild(const SceneNodePtr & child);
	void removeFromParent();

	const glm::mat4 & matrix() const;
	const glm::mat4 & inverseMatrix() const;

	glm::mat4 worldMatrix() const;

	inline const glm::vec3 & position() const noexcept { return m_Position; }
	inline const glm::quat & rotation() const noexcept { return m_Rotation; }
	inline const glm::vec3 & scale() const noexcept { return m_Scale; }

	inline void setPosition(const glm::vec3 & pos) noexcept
		{ m_Position = pos; m_MatrixValid = m_InverseMatrixValid = false; }
	inline void setRotation(const glm::quat & rot) noexcept
		{ m_Rotation = rot; m_MatrixValid = m_InverseMatrixValid = false; }
	inline void setScale(const glm::vec3 & scl) noexcept
		{ m_Scale = scl; m_MatrixValid = m_InverseMatrixValid = false; }

	void update(double time);
	void render(const SceneCamera & camera, const glm::mat4 & parentMatrix = glm::mat4()) const;

	virtual float distanceOnRay(const Ray & ray) const;

protected:
	inline bool matrixValid() const noexcept { return m_MatrixValid; }

	virtual void onUpdate(double time);
	virtual void onRender(const SceneCamera & camera, const glm::mat4 & matrix) const;
	virtual void onMatrixChanged();

private:
	SceneNodeWeakPtr m_Parent;
	std::vector<SceneNodePtr> m_Children;
	glm::vec3 m_Position;
	glm::quat m_Rotation;
	glm::vec3 m_Scale;
	mutable glm::mat4 m_Matrix;
	mutable glm::mat4 m_InverseMatrix;
	mutable bool m_MatrixValid;
	mutable bool m_InverseMatrixValid;

	SceneNode(const SceneNode &) = delete;
	SceneNode & operator=(const SceneNode &) = delete;
};

#endif
