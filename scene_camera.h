/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __cf8528025353d34bfa816cbec0c614af__
#define __cf8528025353d34bfa816cbec0c614af__

#include "scene_node.h"
#include <yip-imports/glm/glm.hpp>

class SceneCamera : public SceneNode
{
public:
	SceneCamera();
	~SceneCamera();

	glm::mat4 viewMatrix() const noexcept;
	const glm::mat4 & projectionMatrix() const noexcept;

	inline bool isOrthographic() const noexcept { return m_Orthographic; }
	inline float fieldOfView() const noexcept { return m_FieldOfView; }
	inline float nearZ() const noexcept { return m_NearZ; }
	inline float farZ() const noexcept { return m_FarZ; }
	inline float aspectRatio() const noexcept { return m_AspectRatio; }

	void setIsOrthographic(bool flag) noexcept;
	void setFieldOfView(float fov) noexcept;
	void setNearZ(float z) noexcept;
	void setFarZ(float z) noexcept;
	void setAspectRatio(float ratio) noexcept;

	void lookAt(const glm::vec3 & target, const glm::vec3 & up = glm::vec3(0.0f, 1.0f, 0.0f));

private:
	bool m_Orthographic;
	float m_FieldOfView;
	float m_AspectRatio;
	float m_NearZ;
	float m_FarZ;
	mutable glm::mat4 m_InverseMatrix;
	mutable glm::mat4 m_ProjectionMatrix;
	mutable bool m_ProjectionMatrixValid;

	SceneCamera(const SceneCamera &) = delete;
	SceneCamera & operator=(const SceneCamera &) = delete;
};

typedef std::shared_ptr<SceneCamera> SceneCameraPtr;

#endif
