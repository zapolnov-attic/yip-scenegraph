/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "scene_node.h"
#include <yip-imports/cxx-util/macros.h>

SceneNode::SceneNode()
	: m_Scale(1.0f, 1.0f, 1.0f),
	  m_MatrixValid(true),
	  m_InverseMatrixValid(true)
{
}

SceneNode::~SceneNode()
{
}

void SceneNode::addChild(const SceneNodePtr & child)
{
	child->removeFromParent();
	m_Children.push_back(child);
	child->m_Parent = shared_from_this();
}

void SceneNode::removeChild(const SceneNodePtr & child)
{
	for (auto it = m_Children.begin(); it != m_Children.end(); ++it)
	{
		if (*it == child)
		{
			(*it)->m_Parent.reset();
			m_Children.erase(it);
			return;
		}
	}
}

void SceneNode::removeFromParent()
{
	SceneNodePtr parentNode = parent();
	if (parentNode.get())
		parentNode->removeChild(shared_from_this());
}

const glm::mat4 & SceneNode::matrix() const
{
	if (LIKELY(m_MatrixValid))
		return m_Matrix;

	m_Matrix = glm::translate(m_Position) * glm::mat4_cast(m_Rotation) * glm::scale(m_Scale);
	m_MatrixValid = true;

	const_cast<SceneNode *>(this)->onMatrixChanged();

	return m_Matrix;
}

const glm::mat4 & SceneNode::inverseMatrix() const
{
	if (LIKELY(m_InverseMatrixValid))
		return m_InverseMatrix;

	m_InverseMatrix = glm::inverse(matrix());
	m_InverseMatrixValid = true;

	return m_Matrix;
}

glm::mat4 SceneNode::worldMatrix() const
{
	SceneNodePtr parentNode = parent();
	if (parentNode.get())
		return parentNode->worldMatrix() * matrix();
	return matrix();
}

void SceneNode::update(double time)
{
	(void)time;
}

void SceneNode::render(const SceneCamera & camera, const glm::mat4 & parentMatrix) const
{
	glm::mat4 m = parentMatrix * matrix();
	onRender(camera, m);
	for (const SceneNodePtr & child : m_Children)
		child->render(camera, m);
}

float SceneNode::distanceOnRay(const Ray & ray) const
{
	return -1.0f;
}

void SceneNode::onUpdate(double time)
{
	(void)time;
}

void SceneNode::onRender(const SceneCamera & camera, const glm::mat4 & matrix) const
{
	(void)camera;
	(void)matrix;
}

void SceneNode::onMatrixChanged()
{
}
