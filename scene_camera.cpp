/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "scene_camera.h"
#include <yip-imports/cxx-util/macros.h>
#include <cassert>

SceneCamera::SceneCamera()
	: m_Orthographic(false),
	  m_FieldOfView(45.0f),
	  m_AspectRatio(1.0f),
	  m_NearZ(1.0f),
	  m_FarZ(100.0f),
	  m_ProjectionMatrixValid(false)
{
}

SceneCamera::~SceneCamera()
{
}

glm::mat4 SceneCamera::viewMatrix() const noexcept
{
	glm::mat4 matrix = inverseMatrix();

	SceneNodePtr parentNode = parent();
	if (!parentNode)
		return matrix;

	do
	{
		matrix = parentNode->inverseMatrix() * matrix;
		parentNode = parentNode->parent();
	}
	while (parentNode);

	return matrix;
}

const glm::mat4 & SceneCamera::projectionMatrix() const noexcept
{
	if (LIKELY(m_ProjectionMatrixValid))
		return m_ProjectionMatrix;

	if (m_Orthographic)
		m_ProjectionMatrix = glm::ortho(-m_AspectRatio, m_AspectRatio, -1.0f, 1.0f, m_NearZ, m_FarZ);
	else
		m_ProjectionMatrix = glm::perspective(m_FieldOfView * 3.1415f / 180.0f, m_AspectRatio, m_NearZ, m_FarZ);
	m_ProjectionMatrixValid = true;

	return m_ProjectionMatrix;
}

void SceneCamera::setIsOrthographic(bool flag) noexcept
{
	if (m_Orthographic != flag)
	{
		m_Orthographic = flag;
		m_ProjectionMatrixValid = false;
	}
}

void SceneCamera::setFieldOfView(float fov) noexcept
{
	if (m_FieldOfView != fov)
	{
		m_FieldOfView = fov;
		m_ProjectionMatrixValid = false;
	}
}

void SceneCamera::setNearZ(float z) noexcept
{
	if (m_NearZ != z)
	{
		m_NearZ = z;
		m_ProjectionMatrixValid = false;
	}
}

void SceneCamera::setFarZ(float z) noexcept
{
	if (m_FarZ != z)
	{
		m_FarZ = z;
		m_ProjectionMatrixValid = false;
	}
}

void SceneCamera::setAspectRatio(float ratio) noexcept
{
	if (m_AspectRatio != ratio)
	{
		m_AspectRatio = ratio;
		m_ProjectionMatrixValid = false;
	}
}

void SceneCamera::lookAt(const glm::vec3 & target, const glm::vec3 & up)
{
	glm::vec3 zaxis = glm::normalize(position() - target);
	glm::vec3 xaxis = glm::normalize(glm::cross(up, zaxis));
	glm::vec3 yaxis = glm::cross(zaxis, xaxis);

	glm::mat3 m(xaxis, yaxis, zaxis);
	setRotation(glm::quat_cast(m));
}
