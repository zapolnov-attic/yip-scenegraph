/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "scene_model.h"
#include "scene_camera.h"
#include <yip-imports/gl_program_binder.h>
#include <yip-imports/gl_buffer_binder.h>
#include <yip-imports/gl_enable_vertex_attrib.h>
#include <yip-imports/cxx-util/on_scope_exit.h>

SceneModel::SceneModel()
	: m_ProgramChanged(false),
	  m_DepthMask(GL::TRUE),
	  m_DepthTest(true),
	  m_ProjectionMatrixUniformName("u_projection"),
	  m_ModelViewMatrixUniformName("u_modelview"),
	  m_NormalMatrixUniformName("u_normal_matrix"),
	  m_CameraPositionUniformName("u_camera_position"),
	  m_MaterialAmbientUniformName("u_material_ambient"),
	  m_MaterialDiffuseUniformName("u_material_diffuse"),
	  m_MaterialSpecularUniformName("u_material_specular"),
	  m_MaterialShininessUniformName("u_material_shininess"),
	  m_MaterialOpacityUniformName("u_material_opacity"),
	  m_TextureUniformName("u_texture"),
	  m_NormalMapUniformName("u_bump_map"),
	  m_PositionAttributeName("a_position"),
	  m_NormalAttributeName("a_normal"),
	  m_TexCoordAttributeName("a_texcoord"),
	  m_TangentAttributeName("a_tangent"),
	  m_BinormalAttributeName("a_binormal")
{
}

SceneModel::~SceneModel()
{
}

void SceneModel::onRender(const SceneCamera & camera, const glm::mat4 & matrix) const
{
	if (UNLIKELY(!m_Program || !m_Model))
		return;

	if (UNLIKELY(m_ProgramChanged))
	{
		m_ProjectionMatrixUniform = m_Program->getUniformLocation(m_ProjectionMatrixUniformName);
		m_ModelViewMatrixUniform = m_Program->getUniformLocation(m_ModelViewMatrixUniformName);
		m_NormalMatrixUniform = m_Program->getUniformLocation(m_NormalMatrixUniformName);
		m_CameraPositionUniform = m_Program->getUniformLocation(m_CameraPositionUniformName);
		m_MaterialAmbientUniform = m_Program->getUniformLocation(m_MaterialAmbientUniformName);
		m_MaterialDiffuseUniform = m_Program->getUniformLocation(m_MaterialDiffuseUniformName);
		m_MaterialSpecularUniform = m_Program->getUniformLocation(m_MaterialSpecularUniformName);
		m_MaterialShininessUniform = m_Program->getUniformLocation(m_MaterialShininessUniformName);
		m_MaterialOpacityUniform = m_Program->getUniformLocation(m_MaterialOpacityUniformName);
		m_TextureUniform = m_Program->getUniformLocation(m_TextureUniformName);
		m_NormalMapUniform = m_Program->getUniformLocation(m_NormalMapUniformName);
		m_PositionAttribute = m_Program->getAttribLocation(m_PositionAttributeName);
		m_NormalAttribute = m_Program->getAttribLocation(m_NormalAttributeName);
		m_TexCoordAttribute = m_Program->getAttribLocation(m_TexCoordAttributeName);
		m_TangentAttribute = m_Program->getAttribLocation(m_TangentAttributeName);
		m_BinormalAttribute = m_Program->getAttribLocation(m_BinormalAttributeName);
		m_ProgramChanged = false;
	}

	if (UNLIKELY(m_PositionAttribute < 0))
		return;

	GL::BufferBinder binder1(m_Model->indexBuffer(), GL::ELEMENT_ARRAY_BUFFER);
	GL::ProgramBinder binder2(m_Program);

	m_Model->bindVertexBuffer(m_PositionAttribute, m_TexCoordAttribute, m_NormalAttribute,
		m_TangentAttribute, m_BinormalAttribute);

	GL::EnableVertexAttrib enable1(m_PositionAttribute);
	GL::EnableVertexAttrib enable2(m_NormalAttribute);
	GL::EnableVertexAttrib enable3(m_TexCoordAttribute);
	GL::EnableVertexAttrib enable4(m_TangentAttribute);
	GL::EnableVertexAttrib enable5(m_BinormalAttribute);

	if (LIKELY(m_ProjectionMatrixUniform >= 0))
		GL::uniformMatrix4fv(m_ProjectionMatrixUniform, 1, GL::FALSE, &camera.projectionMatrix()[0][0]);
	if (LIKELY(m_ModelViewMatrixUniform >= 0))
		GL::uniformMatrix4fv(m_ModelViewMatrixUniform, 1, GL::FALSE, &matrix[0][0]);

	if (m_NormalMatrixUniform >= 0)
	{
		glm::mat3 mat = glm::mat3(glm::transpose(glm::inverse(matrix)));
		GL::uniformMatrix3fv(m_NormalMatrixUniform, 1, GL::FALSE, &mat[0][0]);
	}

	if (m_CameraPositionUniform >= 0)
		GL::uniform3fv(m_CameraPositionUniform, 1, &camera.matrix()[3][0]);

	OnScopeExit cleanup([](){
		GL::activeTexture(GL::TEXTURE1);
		GL::bindTexture(GL::TEXTURE_CUBE_MAP, 0);
		GL::bindTexture(GL::TEXTURE_2D, 0);
		GL::activeTexture(GL::TEXTURE0);
		GL::bindTexture(GL::TEXTURE_CUBE_MAP, 0);
		GL::bindTexture(GL::TEXTURE_2D, 0);
	});

	GL::depthMask(m_DepthMask);

	if (m_DepthTest)
		GL::enable(GL::DEPTH_TEST);
	else
		GL::disable(GL::DEPTH_TEST);

	const GL::Model::Material * lastMaterial = nullptr;
	for (int i = 0; i < m_Model->numMeshes(); i++)
	{
		const GL::Model::Material * material = m_Model->mesh(i).material;
		if (material != lastMaterial)
		{
			lastMaterial = material;

			GL::activeTexture(GL::TEXTURE0);
			if (!material->texture.get())
			{
				GL::bindTexture(GL::TEXTURE_CUBE_MAP, 0);
				GL::bindTexture(GL::TEXTURE_2D, 0);
			}
			else if (material->texture->target() == GL::TEXTURE_2D)
			{
				GL::bindTexture(GL::TEXTURE_CUBE_MAP, 0);
				GL::bindTexture(GL::TEXTURE_2D, material->texture->handle());
			}
			else if (material->texture->target() == GL::TEXTURE_CUBE_MAP)
			{
				GL::bindTexture(GL::TEXTURE_2D, 0);
				GL::bindTexture(GL::TEXTURE_CUBE_MAP, material->texture->handle());
			}

			GL::activeTexture(GL::TEXTURE1);
			if (!material->normalMap.get())
			{
				GL::bindTexture(GL::TEXTURE_CUBE_MAP, 0);
				GL::bindTexture(GL::TEXTURE_2D, 0);
			}
			else if (material->normalMap->target() == GL::TEXTURE_2D)
			{
				GL::bindTexture(GL::TEXTURE_CUBE_MAP, 0);
				GL::bindTexture(GL::TEXTURE_2D, material->normalMap->handle());
			}
			else if (material->normalMap->target() == GL::TEXTURE_CUBE_MAP)
			{
				GL::bindTexture(GL::TEXTURE_2D, 0);
				GL::bindTexture(GL::TEXTURE_CUBE_MAP, material->normalMap->handle());
			}

			if (m_TextureUniform >= 0)
				GL::uniform1i(m_TextureUniform, 0);
			if (m_NormalMapUniform >= 0)
				GL::uniform1i(m_NormalMapUniform, 1);

			if (m_MaterialAmbientUniform >= 0)
				GL::uniform4fv(m_MaterialAmbientUniform, 1, &material->ambient[0]);
			if (m_MaterialDiffuseUniform >= 0)
				GL::uniform4fv(m_MaterialDiffuseUniform, 1, &material->diffuse[0]);
			if (m_MaterialSpecularUniform >= 0)
				GL::uniform4fv(m_MaterialSpecularUniform, 1, &material->specular[0]);
			if (m_MaterialShininessUniform >= 0)
				GL::uniform1f(m_MaterialShininessUniform, material->shininess);
			if (m_MaterialOpacityUniform >= 0)
				GL::uniform1f(m_MaterialOpacityUniform, material->opacity);
		}

		m_Model->drawMesh(i);
	}
}
