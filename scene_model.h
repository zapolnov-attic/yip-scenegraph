/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __2234b01ceaf4d6b75e77b0adadbe7b61__
#define __2234b01ceaf4d6b75e77b0adadbe7b61__

#include "scene_node.h"
#include <yip-imports/gl_model.h>
#include <string>

class SceneModel : public SceneNode
{
public:
	SceneModel();
	~SceneModel();

	inline const GL::ProgramPtr & program() const noexcept { return m_Program; }
	inline const GL::ModelPtr & model() const noexcept { return m_Model; }

	inline void setDepthMask(GL::Boolean mask) noexcept { m_DepthMask = mask; }
	inline void setDepthTest(bool flag) noexcept { m_DepthTest = flag; }

	inline void setProgram(const GL::ProgramPtr & program) noexcept
		{ m_Program = program; m_ProgramChanged = true; }

	inline void setProjectionMatrixUniformName(const std::string & name)
		{ m_ProjectionMatrixUniformName = name; m_ProgramChanged = true; }
	inline void setModelViewMatrixUniformName(const std::string & name)
		{ m_ModelViewMatrixUniformName = name; m_ProgramChanged = true; }
	inline void setNormalMatrixUniformName(const std::string & name)
		{ m_NormalMatrixUniformName = name; m_ProgramChanged = true; }
	inline void setCameraPositionUniformName(const std::string & name)
		{ m_CameraPositionUniformName = name; m_ProgramChanged = true; }
	inline void setMaterialAmbientUniformName(const std::string & name)
		{ m_MaterialAmbientUniformName = name; m_ProgramChanged = true; }
	inline void setMaterialDiffuseUniformName(const std::string & name)
		{ m_MaterialDiffuseUniformName = name; m_ProgramChanged = true; }
	inline void setMaterialSpecularUniformName(const std::string & name)
		{ m_MaterialSpecularUniformName = name; m_ProgramChanged = true; }
	inline void setMaterialShininessUniformName(const std::string & name)
		{ m_MaterialShininessUniformName = name; m_ProgramChanged = true; }
	inline void setMaterialOpacityUniformName(const std::string & name)
		{ m_MaterialOpacityUniformName = name; m_ProgramChanged = true; }
	inline void setTextureUniformName(const std::string & name)
		{ m_TextureUniformName = name; m_ProgramChanged = true; }
	inline void setNormalMapUniformName(const std::string & name)
		{ m_NormalMapUniformName = name; m_ProgramChanged = true; }

	inline void setPositionAttributeName(const std::string & name)
		{ m_PositionAttributeName = name; m_ProgramChanged = true; }
	inline void setNormalAttributeName(const std::string & name)
		{ m_NormalAttributeName = name; m_ProgramChanged = true; }
	inline void setTexCoordAttributeName(const std::string & name)
		{ m_TexCoordAttributeName = name; m_ProgramChanged = true; }
	inline void setTangentAttributeName(const std::string & name)
		{ m_TangentAttributeName = name; m_ProgramChanged = true; }
	inline void setBinormalAttributeName(const std::string & name)
		{ m_BinormalAttributeName = name; m_ProgramChanged = true; }

	inline void setModel(const GL::ModelPtr & model) noexcept { m_Model = model; }

protected:
	void onRender(const SceneCamera & camera, const glm::mat4 & matrix) const override;

private:
	GL::ProgramPtr m_Program;
	GL::ModelPtr m_Model;
	GL::Boolean m_DepthMask;
	bool m_DepthTest;
	std::string m_ProjectionMatrixUniformName;
	std::string m_ModelViewMatrixUniformName;
	std::string m_NormalMatrixUniformName;
	std::string m_CameraPositionUniformName;
	std::string m_MaterialAmbientUniformName;
	std::string m_MaterialDiffuseUniformName;
	std::string m_MaterialSpecularUniformName;
	std::string m_MaterialShininessUniformName;
	std::string m_MaterialOpacityUniformName;
	std::string m_TextureUniformName;
	std::string m_NormalMapUniformName;
	std::string m_PositionAttributeName;
	std::string m_NormalAttributeName;
	std::string m_TexCoordAttributeName;
	std::string m_TangentAttributeName;
	std::string m_BinormalAttributeName;
	mutable int m_ProjectionMatrixUniform;
	mutable int m_ModelViewMatrixUniform;
	mutable int m_NormalMatrixUniform;
	mutable int m_CameraPositionUniform;
	mutable int m_MaterialAmbientUniform;
	mutable int m_MaterialDiffuseUniform;
	mutable int m_MaterialSpecularUniform;
	mutable int m_MaterialShininessUniform;
	mutable int m_MaterialOpacityUniform;
	mutable int m_TextureUniform;
	mutable int m_NormalMapUniform;
	mutable int m_PositionAttribute;
	mutable int m_NormalAttribute;
	mutable int m_TexCoordAttribute;
	mutable int m_TangentAttribute;
	mutable int m_BinormalAttribute;
	mutable bool m_ProgramChanged;

	SceneModel(const SceneModel &) = delete;
	SceneModel & operator=(const SceneModel &) = delete;
};

typedef std::shared_ptr<SceneModel> SceneModelPtr;

#endif
